---
author: "Gius. Camerlingo"
date: 2018-11-19
title: "Hello Hugo"
description: "Make the web fun again"
meta_img: "/img/hugo.png"
tags: [hugo]
---

Each person that wants to publish his/her own _blog/website/portfolio_ (or whatever) should really consider the idea to learn how to use a [static site generator](https://www.staticgen.com/) to speed up the development and the deployment and focus just on his/her own _ideas/thoughts/projects_. 

### **KISS**

I personally choosed from a bunch of different solutions: [Hugo](https://gohugo.io/). It's an open source framework written in [Go](https://golang.org/).

![hugo](/img/hugo.png)

> _So should I learn **go** to start?_

Absolutely not! **Hugo** is for `text-editor` lovers that like to write in `markdown`.

Once you choosed your [theme](https://themes.gohugo.io/), set your `config.toml` and get the right confidence with it, you can easily break the ice firing up your new project:

```shell
$ hugo server # will normally serve your project at localhost:1313
```

That's awesome, isn't it? Here you can find the official [quick start](https://gohugo.io/getting-started/quick-start/) guide to follow this process step by step. So hurry up and [keep it simple](https://en.wikipedia.org/wiki/KISS_principle).

If you search for some inspiration you can take a look at the [repo](https://gitlab.com/gcamerli/ramblings) of this project.

---

Happy hacking!

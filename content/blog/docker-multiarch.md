---
author: "Gius. Camerlingo"
date: 2018-12-12
title: "Is Docker multi-arch?"
description: "Multi-arch all the things"
meta_img: "/img/docker-arm.png"
tags: [docker, arm]
---

In november 2017 [Phil Estes](https://www.docker.com/captains/phil-estes) in the post [Multi-arch all the things](https://blog.docker.com/2017/11/multi-arch-all-the-things/) ironically stated:

> The containerization craze has grown like wildfire since then, but left us back in a Henry Ford-esque conundrum: “you can run a container anywhere you want, as long as it is the x86_64 CPU architecture on Linux.”

For those who have tried at least once to run Docker on a **Raspberry Pi** soon enough the feeling that something is missing comes to an end: "_This architecture is supported?_"

![docker-arm](/img/docker-arm.png)

In my last personal experience I learned how it can be simple to avoid that feeling and continue to build what you love without pain.

### **Official Images**

All the [official images](https://docs.docker.com/docker-hub/official_repos/) published on [Docker Hub](https://hub.docker.com/explore/) are supported for several kind of architectures other than `amd64` (X86_64). In particular as it is specified on the github repo of [docker-library](https://github.com/docker-library/official-images#architectures-other-than-amd64) Docker supports officially:

+ [`arm32v7`](https://hub.docker.com/u/arm32v7/)
+ [`arm64v8`](https://hub.docker.com/u/arm64v8/)
+ [`windows-amd64`](https://hub.docker.com/u/winamd64/)

Other architectures are not officially supported by Docker but widely used in the official repos. This practically means that if I have to call an official image in my `Dockerfile` I don't need to change anything and Docker will do his job.

```docker
FROM nginx:stable
```

So for example calling the `nginx` image will work in the same manner on each kind of supported architecture.

![wow](https://media.giphy.com/media/YlPeYXasYEPpC/giphy.gif)

### **Custom Images**

"_What if I have to build a custom image?_"

The scenario it's pretty the same also if I need to build a custom `Dockerfile`. I can build locally on a specific architecture calling the same official image as the first **layer**, just waiting the end of the build.

The troubles come when it's necessary to build **multiple** images at the same time for a more complex **stack** or the physical resources of our work environment are not enough.

So because we never like to wait, to speed up the build process a good workaround can be to specify the architecture as the following example:

```docker
FROM arm64v8/nginx:stable
```

and using **qemu** to emulate it on any x86_64 arch. Cool, isn't it?

### **Qemu**

[Qemu](https://www.qemu.org/) is an open source machine emulator and virtualizer that can enhance the baking of our custom images for several architectures.

It's enough to install on your **host** system `qemu` and `qemu-user-static` to build and run whatever docker images without troubles.

To go further and make your images portable it's necessary to copy inside your `Dockerfile` this binary:

```docker
COPY qemu-arm-static /usr/bin/
```

Enjoy the simplicity, that's all.

---

Happy hacking!

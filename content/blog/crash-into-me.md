---
author: "Gius. Camerlingo"
date: 2019-01-01
title: "Crash Into Me"
description: "My 2018 in review"
meta_img: "/img/ocean-rocks.jpg"
tags: [review, year]
---

> _All men dream: but not equally. Those who dream by night in the dusty recesses of their minds wake in the day to find that it was vanity: but the dreamers of the day are dangerous men, for they may act their dreams with open eyes, to make it possible. This I did._ 

> (T. E. Lawrence, Seven Pillars of Wisdom)

![waves](/img/ocean-rocks.jpg)

During the **2018** I learned more about myself than I could ever imagine before. I want just to thank you all the **people** that made it possible: friends, lovers, haters, unknown or less known people. Each one of them added something useful during this year that now turns into a new one.

There is no good or evil lesson when your **time** it’s what is worth. So I wish all these people for the new year the same of what I did in the previous one. Stay awake and never stop to **learn**.

Have a good one

---
title: "Cv"
date: 2018-11-15
---

You can find an up to date version of my personal cv [here](https://blog.gcamer.li/doc/gcamerli.pdf).

Don't hesitate to let me know about new opportunities. I like to keep things moving.

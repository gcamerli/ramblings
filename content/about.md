+++
date = "2018-11-15"
title = "About"
+++

> _My work is a game, a very serious game._
>
> M. C. Escher

Hi, I’m a **software engineer**, always eager to learn, strongly interested in **distributed systems**, **cloud native** architectures, **programming** and not least **chaos engineering**. I’m always thrilled to be part of new **technical challenges**, where I can grow my skills, myself and my personal vision of the world. **Open source** is my red pill.

When I don't have my hands on my favorite keyboards I like to keep my **privacy** on. Drop me a line if you want to keep in touch.

Here it is my **[pgp](https://keybase.io/gcamerli/pgp_keys.asc)** key.

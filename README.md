# Ramblings

_A personal journey inside the blogsphere_ [ **[blog.gcamer.li](https://blog.gcamer.li)** ].

[![pipeline status](https://gitlab.com/gcamerli/ramblings/badges/master/pipeline.svg)](https://gitlab.com/gcamerli/ramblings/commits/master)

![heart_of_gold](img/heart_of_gold.gif)

### **Description**

This is my personal blog. Opinions are mine.

### **Credits**

This blog is built with [Hugo](https://gohugo.io) and hosted by [Gitlab](https://gitlab.com) with love.

**Theme**: [Cocoa Enhanced](https://github.com/mtn/cocoa-eh-hugo-theme)

### **CC License**

This work is provided under the terms of this license: [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/)
